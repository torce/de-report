pdf:
	pdflatex memoria.tex

slides:
	pdflatex slides.tex

clean:
	rm -f *out *log *~ *aux *bbl *bcf *blg *idx *ilg *ind *ptc *run.xml *toc *lof *lot

mrproper: clean
	rm -f memoria.pdf

all: pdf slides
	makeindex memoria.idx -s style-ind.ist
	biber memoria
	pdflatex memoria.tex
	pdflatex memoria.tex

